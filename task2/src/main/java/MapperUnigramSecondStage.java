import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Mapper class.
 */
public class MapperUnigramSecondStage extends Mapper<Object, Text, IntWritable, Unigram> {
    private final static String stringRegexMapper = "(.*?\\s+)(.*?\\s+)(.*?\\s+)(.*?\\s+)(.*)";
    private IntWritable decadeOut = new IntWritable();

    /**
     * Get line by line the input file.
     * Write in the context, the information of each line of the file, but with the decade and check if the first unigram
     * starts with the best of the decade.
     * [Decade, Unigram(occurs, word1 word2 )]
     * @param key Object with input information.
     * @param value Line information.
     * @param context Context to set the information to next stage -> [Decade, Unigram(occurs, word)].
     */
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        Matcher m = Pattern.compile(stringRegexMapper).matcher(value.toString());
        m.find();

        String actualFirstNgram = m.group(1).trim();
        String actualSecondNgram = m.group(2).trim();
        int actualYear = Integer.parseInt(m.group(3).trim());
        int actualDecade = actualYear-(actualYear%10);

        if (actualYear >= 1900 && actualYear < 2000) {
            String best1gram = context.getConfiguration().get(String.valueOf(actualDecade));
            if (best1gram.equals(actualFirstNgram)) {
                int actualOccurs = Integer.parseInt(m.group(4).trim());
                decadeOut.set(actualDecade);
                context.write(decadeOut, new Unigram(actualOccurs, actualFirstNgram + " " + actualSecondNgram));
            }
        }
    }
}