import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Mapper class.
 */
public class MapperUnigramFirstStage extends Mapper<Object, Text, IntWritable, Unigram> {
    private final static String stringRegexMapper = "(.*?\\s+)(.*?\\s+)(.*?\\s+)(.*)";
    private IntWritable decadeOut = new IntWritable();

    /**
     * Get line by line the input file.
     * Write in the context, the information of each line of the file, but with the decade.
     * [Decade, Unigram(occurs, word)]
     * @param key Object with input information.
     * @param value Line information.
     * @param context Context to set the information to next stage -> [Decade, Unigram(occurs, word)].
     */
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        Matcher m = Pattern.compile(stringRegexMapper).matcher(value.toString());
        m.find();

        String actualNgram = m.group(1).trim();
        int actualYear = Integer.parseInt(m.group(2).trim());

        if (StringUtils.stripAccents(actualNgram).toLowerCase().startsWith("a") && actualYear >= 1800) {
            int actualOccurs = Integer.parseInt(m.group(3).trim());
            int actualDecade = actualYear - (actualYear % 10);
            decadeOut.set(actualDecade);
            context.write(decadeOut, new Unigram(actualOccurs, actualNgram));
        }
    }
}