import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task2 {

    private static final String ngramfile1 = "googlebooks-spa-all-1gram-20120701-a";
    private static final String ngramfile2 = "googlebooks-spa-all-2gram-20120701-";
    private static final String temFile = "/tmp/secondStage";

    /**
     * Main class and 1 job.
     * @param args [0] input directory [1] output directory.
     */
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Arguments: <input> <output>");
            System.exit(1);
        }

        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Task2 - version 1 - 1gram count");

        FileSystem fs1 = FileSystem.get(new URI(ngramfile1), conf);
        fs1.delete(new Path(temFile));

        // Set class
        job.setJarByClass(Task2.class);
        job.setMapperClass(MapperUnigramFirstStage.class);
        job.setCombinerClass(ReducerStages.class);
        job.setReducerClass(ReducerStages.class);

        // Specify the type of output keys and values
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Unigram.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Unigram.class);

        // Set files (input - output)
        FileInputFormat.addInputPath(job, new Path(args[0] + "/" + ngramfile1));
        FileOutputFormat.setOutputPath(job, new Path(temFile));
        int code = job.waitForCompletion(true) ? 0 : 1;

        if (code == 0) {
            conf = getBestResults(conf, temFile + "/part-r-00000");
            Job secondJob = Job.getInstance(conf);
            secondJob.setJobName("Task2 - Second Stage");

            // Set class
            secondJob.setJarByClass(Task2.class);
            secondJob.setMapperClass(MapperUnigramSecondStage.class);
            secondJob.setCombinerClass(ReducerStages.class);
            secondJob.setReducerClass(ReducerStages.class);

            // Specify the type of output keys and values
            secondJob.setMapOutputKeyClass(IntWritable.class);
            secondJob.setMapOutputValueClass(Unigram.class);
            secondJob.setOutputKeyClass(IntWritable.class);
            secondJob.setOutputValueClass(Unigram.class);

            ArrayList<String> prefixsNgram = new ArrayList<>();
            for (int x = 190; x < 200; x++ ){
                String ngram = StringUtils.stripAccents(conf.get(x + "0"));
                if(ngram != null && !prefixsNgram.contains(ngram)){
                    prefixsNgram.add(ngram);
                    ngram = ngram + "_";
                    FileInputFormat.addInputPath(secondJob, new Path(args[0] + "/" + ngramfile2 + ngram.substring(0,2)));
                }
            }
            FileOutputFormat.setOutputPath(secondJob, new Path(args[1]));
            secondJob.waitForCompletion(true);
        } else {
            System.exit(1);
        }
    }

    /**
     * Get the best ngrams from the temporal file.
     * @param conf Configuration
     * @param fileName Temporal file
     * @return Configuration with the best words-decade tuples.
     */
    public static Configuration getBestResults(Configuration conf, String fileName){
        try {
            String stringRegexMapper = "(.*?\\s+)(.*?\\s+)(.*)";
            Path pt = new Path(fileName);
            FileSystem fs = FileSystem.get(new Configuration());
            BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt), "UTF-8"));
            String line = br.readLine();
            while (line != null) {
                Matcher m = Pattern.compile(stringRegexMapper).matcher(line);
                m.find();
                if (Integer.parseInt(m.group(1).trim()) >= 1900 && Integer.parseInt(m.group(1).trim()) < 2000){
                    conf.set(m.group(1).trim(), m.group(2).trim());
                }
                line = br.readLine();
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return conf;
    }
}