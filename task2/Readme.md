## TASK 2 - LOCAL PSEUDODISTRIBUTED

### Hadoop installation

- I run a pseudodistributed hadoop in a ubuntu:16.04 docker instance.

- I add to the delivery all the automation scripts (hadoop + dockerfiles).

### Mapreduce algorithm

- First Stage:

 Like the task1, in the mapper , I pass each line of the google 1gram file with the correct decade, for example

```
  1981 a 23    ->   1980 a 23 
  1982 a 45    ->   1980 a 45
  1997 a 2     ->   1990 a 2
  1988 a 27    ->   1990 a 27
```

 In the reducer, add all the equals words in the same decade and choose the best (if tie, alphabetical order):

```
  1980 [a 23, a 45, arbol 34, ... ] -> 1980 [a 68, arbol 34, .. ] -> 1980 a 68
```

This result is stored in a temporal file.


- Second Stage:

Check the most repeat words in the result file, and then call the second mapper with the best words per decade in tuples, in the Configuration class. Also get the required or requireds files in the input folder (bigram files).

Then in the mapper check if the first ngram is the most repeat and put the lines like the first mapper:

```
A the most repeat:

  1981 a arbol 23    ->   1980 a arbol 23 
  1982 a arbol 45    ->   1980 a arbol 45
  1997 a mesa  2     ->   1990 a mesa  2
  1988 b arbol 27    x
```

The reduce is like the first stage.




### Result TASK 3

```
1900	á _DET_	10479806
1910	a _DET_	8206832
1920	a _DET_	15653312
1930	a _DET_	20580790
1940	a _DET_	31622106
1950	a _DET_	37577188
1960	a _DET_	56117998
1970	a _DET_	71739051
1980	a _DET_	88812732
1990	a _DET_	112580535
```

- Time local pseudodistributed: 5:00 minutes