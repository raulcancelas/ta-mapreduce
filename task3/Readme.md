## TASK 3 - AWS PSEUDODISTRIBUTED

### Hadoop AWS pseudodistributed installation

- I install hadoop in the EC2 instance. The diference with the local instalation is the LZO codec. It is neccesary for the Google Ngrams.

- For avoid memory problems I set a 4 GB swap memory.

- I add in the deliver all the automation scripts.

### LZO configuration

- Install the codec by apt-get: liblzo2-2 liblzo2-dev 

- Create a jar file for Hadoop can use the codec: hadoop-lzo-0.4.21-SNAPSHOT.jar (https://github.com/twitter/hadoop-lzo)

- Configure the local variables

- Set the next properties in `core-site.xml`:

```
<property>
        <name>mapred.input.compression.codec</name>
        <value>com.hadoop.compression.lzo.LzopCodec</value>
    </property>

    <property> 
        <name>io.compression.codecs</name>
        <value>org.apache.hadoop.io.compress.GzipCodec,
            org.apache.hadoop.io.compress.DefaultCodec,
            org.apache.hadoop.io.compress.BZip2Codec,
            com.hadoop.compression.lzo.LzoCodec, com.hadoop.compression.lzo.LzopCodec
        </value>
    </property>
    
    <property> 
        <name>io.compression.codec.lzo.class</name>
        <value>com.hadoop.compression.lzo.LzoCodec</value>
    </property>
```

### S3 configuration

- Set the next properties in `core-site.xml`:

```
    <property>
        <name>fs.s3.impl</name>
        <value>org.apache.hadoop.fs.s3.S3FileSystem</value>
    </property>
    
    <property>
        <name>fs.s3a.access.key</name>
        <value>****</value>
    </property>

    <property>
        <name>fs.s3a.secret.key</name>
        <value>*******</value>
    </property>

```

- In the code, put as input format file `SequenceFileInputFormat`

###Algorithm

- I used the same algorithm that the Task1, but y set the file with `SequenceFileInputFormat`.

```
job.setInputFormatClass(SequenceFileInputFormat.class);
FileInputFormat.addInputPath(job, new Path("s3a://datasets.elasticmapreduce/ngrams/books/20090715/spa-all/1gram/data"));
```

### Results (results_task3.txt)

```
1800	á	1393364
1810	á	867472
1820	á	1836293
1830	á	2490020
1840	á	6102507
1850	á	6902084
1860	á	8093144
1870	á	6311563
1880	á	8798241
1890	á	8766076
1900	á	10559277
1910	a	7103009
1920	a	14661740
1930	a	20680143
1940	a	32240773
1950	a	40804316
1960	a	60014105
1970	a	83380895
1980	a	96059162
1990	a	116070426
2000	a	135203258
```

- Time AWS pseudodistributed: 13:15 minutes