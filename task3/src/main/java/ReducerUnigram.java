import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.*;

/**
 * Reducer class.
 */
public class ReducerUnigram extends Reducer<IntWritable,Unigram,IntWritable,Unigram> {

    /**
     * Get the key (decade) and find the Unigram with the max value.
     * If the value of two ngram are equals, compare it.
     * @param key  Decade.
     * @param values  Unigram: occurs and word]
     * @param context
     */
    public void reduce(IntWritable key, Iterable<Unigram> values, Context context) throws IOException, InterruptedException {
        HashMap<String, Integer> reduceMap = new HashMap<>();

        // Plus all the equals Unigrams
        for (Unigram ngram : values) {
            if(reduceMap.containsKey(ngram.getNgram())) {
                reduceMap.put(ngram.getNgram(), reduceMap.get(ngram.getNgram()) + ngram.getOccurs());
            } else {
                reduceMap.put(ngram.getNgram(), ngram.getOccurs());
            }
        }

        // Get the best Unigram, if these are equals, compare it
        Map.Entry<String, Integer> bestNgram = null;
        for (Map.Entry<String, Integer> actualNgram : reduceMap.entrySet()) {
            if (bestNgram == null ||
                    (actualNgram.getValue() > bestNgram.getValue()) ||
                    (actualNgram.getValue() == bestNgram.getValue() && actualNgram.getKey().compareTo(bestNgram.getKey()) < 0)) {
                bestNgram = actualNgram;
            }
        }

        // Set the results
        if(bestNgram != null) context.write(key, new Unigram(bestNgram.getValue(), bestNgram.getKey()));
    }
}