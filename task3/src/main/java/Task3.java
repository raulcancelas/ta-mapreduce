import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


public class Task3 {

    /**
     * Main class and 1 job.
     * @param args [0] input directory [1] output directory.
     */
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Arguments: <input> <output>");
            System.exit(1);
        }

        Configuration conf = new Configuration();
        conf.set( "dfs.blocksize", String.valueOf(256*1024*1024)) ;

        Job job = Job.getInstance(conf, "Task3 - 1gram count");

        // Set class
        job.setJarByClass(Task3.class);
        job.setMapperClass(MapperUnigram.class);
        job.setCombinerClass(ReducerUnigram.class);
        job.setReducerClass(ReducerUnigram.class);

        // Specify the type of output keys and values
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Unigram.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Unigram.class);

        // Set files (input - output)
        job.setInputFormatClass(SequenceFileInputFormat.class);
        FileInputFormat.addInputPath(job, new Path("s3a://datasets.elasticmapreduce/ngrams/books/20090715/spa-all/1gram/data"));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}