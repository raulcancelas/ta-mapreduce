# TA-HADOOP-CLUSTER
Cluster Hadoop formado por 1 master y 2 slaves.

El archivo dockerfile de la carpeta base crea imagenes docker con hadoop.
El archivo dockerfile de la carpeta master añade lo necesario a la imagen anterior para combertirse en la instancia master.
En la carpeta master/config se encuentran los ficheros de configuración que serán copiados a la carpeta correspondiente en el docker.

Launch:

```
./start-cluster.sh
```

Interfeces web:
- Procesos yarn: http://localhost:8088/
- Hadoop hdfs y cluster: http://localhost:9870/

### SIMPLE TEST (WORDCOUNT)

Ayndando de un jar de ejemplo de hadoop y de el fichero de texto de la carpeta data:

```
yarn jar /usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.2.1.jar wordcount input output
```
