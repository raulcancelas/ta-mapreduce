FROM ubuntu:16.04
MAINTAINER rcf

WORKDIR /root
ARG DEBIAN_FRONTEND=noninteractive

# instalar ssh java y wget
RUN apt-get update && apt-get install -y openssh-server openjdk-8-jdk wget

# instalar hadoop
RUN wget http://apache.rediris.es/hadoop/common/hadoop-3.2.1/hadoop-3.2.1.tar.gz && \
    tar -xzvf hadoop-3.2.1.tar.gz && \
    mv hadoop-3.2.1 /usr/local/hadoop && \
    rm hadoop-3.2.1.tar.gz

RUN apt-get update && apt-get install -yq  liblzo2-2 liblzo2-dev

#gcc make

# instalar lzo
# RUN wget http://www.oberhumer.com/opensource/lzo/download/lzo-2.09.tar.gz && \
#     tar xvfz lzo-2.09.tar.gz && \
#     mv lzo-2.09 /usr/local/hadoop/zlo && \
#     rm lzo-2.09.tar.gz

# RUN cd /usr/local/hadoop/zlo/ && ./configure --enable-shared --prefix /usr/local/lzo-2.09
# RUN cd /usr/local/hadoop/zlo/ && make install

# install hadoop-lzo
#RUN git clone https://github.com/twitter/hadoop-lzo.git /usr/lib/hadoop/lib/hadoop-lzo/ && \
#   mv hadoop-lzo /usr/local/hadoop/
#
#RUN C_INCLUDE_PATH=/usr/local/lzo-2.09/include LIBRARY_PATH=/usr/local/lzo-2.09/lib mvn -f usr/local/hadoop/hadoop-lzo/pom.xml -Dmaven.test.skip=true package
#RUN cp /usr/local/hadoop/hadoop-lzo/target/hadoop-lzo-0.4.20-SNAPSHOT.jar /usr/local/hadoop/lib/

# variables de entorno
ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
ENV HADOOP_CLASSPATH=/usr/lib/jvm/java-8-openjdk-amd64/lib/tools.jar
ENV HADOOP_HOME=/usr/local/hadoop
ENV HADOOP_CLASSPATH=$HADOOP_CLASSPATH:$HADOOP_HOME/share/hadoop/tools/lib/*
ENV PATH=$PATH:/usr/local/hadoop/bin:/usr/local/hadoop/sbin
ENV PATH=$PATH:$HADOOP_HOME/share/hadoop/tools/lib/*
ENV HDFS_NAMENODE_USER="root"
ENV HDFS_DATANODE_USER="root"
ENV HDFS_SECONDARYNAMENODE_USER="root"
ENV YARN_RESOURCEMANAGER_USER="root"
ENV YARN_NODEMANAGER_USER="root"

# configuracion ssh
RUN ssh-keygen -t rsa -f ~/.ssh/id_rsa -P '' && \
    cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys && \
    chmod 0600 ~/.ssh/authorized_keys

# configuracion hdfs
RUN mkdir -p ~/hdfs/namenode && \
    mkdir -p ~/hdfs/datanode && \
    mkdir $HADOOP_HOME/logs

# Copiar ficheros de configuracion
COPY config/* /tmp/

RUN mv /tmp/ssh_config ~/.ssh/config && \
    mv /tmp/hadoop-env.sh /usr/local/hadoop/etc/hadoop/hadoop-env.sh && \
    mv /tmp/hdfs-site.xml $HADOOP_HOME/etc/hadoop/hdfs-site.xml && \
    mv /tmp/core-site.xml $HADOOP_HOME/etc/hadoop/core-site.xml && \
    mv /tmp/mapred-site.xml $HADOOP_HOME/etc/hadoop/mapred-site.xml && \
    mv /tmp/start-hadoop.sh ~/start-hadoop.sh && \
    mv /tmp/hadoop-lzo-0.4.21-SNAPSHOT.jar /usr/local/hadoop/share/hadoop/common/hadoop-lzo-0.4.21-SNAPSHOT.jar

RUN chmod +x ~/start-hadoop.sh && \
    chmod +x $HADOOP_HOME/sbin/start-dfs.sh

CMD [ "sh", "-c", "service ssh start; bash"]

ADD config/bootstrap.sh /etc/bootstrap.sh
RUN chown root:root /etc/bootstrap.sh
RUN chmod 700 /etc/bootstrap.sh

ADD config/configure-hdfs-files.sh /etc/configure-hdfs-files.sh
RUN chown root:root /etc/configure-hdfs-files.sh
RUN chmod 700 /etc/configure-hdfs-files.sh

ADD config/showResults.sh /usr/local/hadoop/code/showResults.sh
RUN chown root:root /usr/local/hadoop/code/showResults.sh
RUN chmod 700 /usr/local/hadoop/code/showResults.sh

ADD config/compileAndExecute1.sh /usr/local/hadoop/code/compileAndExecute1.sh
RUN chown root:root /usr/local/hadoop/code/compileAndExecute1.sh
RUN chmod 700 /usr/local/hadoop/code/compileAndExecute1.sh

ADD config/compileAndExecute2.sh /usr/local/hadoop/code/compileAndExecute2.sh
RUN chown root:root /usr/local/hadoop/code/compileAndExecute2.sh
RUN chmod 700 /usr/local/hadoop/code/compileAndExecute2.sh

ENV BOOTSTRAP /etc/bootstrap.sh

# format namenode
RUN /usr/local/hadoop/bin/hdfs namenode -format

CMD ["/etc/bootstrap.sh", "-d"]

# Hdfs ports
EXPOSE 9000 50010 50020 50070 50075 50090
EXPOSE 9871 9870 9820 9869 9868 9867 9866 9865 9864
# Mapred ports
EXPOSE 19888
#Yarn ports
EXPOSE 8030 8031 8032 8033 8040 8042 8088 8188
#Other ports
EXPOSE 49707 2122
