#!/bin/bash
hdfs dfs -rm -r /user/root/output
hadoop com.sun.tools.javac.Main Task2.java MapperUnigramFirstStage.java MapperUnigramSecondStage.java ReducerStages.java Unigram.java
jar cf task2.jar Task2.class MapperUnigramFirstStage.class MapperUnigramSecondStage.class ReducerStages.class Unigram.class
hadoop jar task2.jar Task2 /user/root/input /user/root/output