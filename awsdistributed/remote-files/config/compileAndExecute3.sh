#!/bin/bash
hdfs dfs -rm -r /user/root/output
hadoop com.sun.tools.javac.Main Task3.java MapperUnigram.java ReducerUnigram.java Unigram.java
jar cf task3.jar Task3.class MapperUnigram.class ReducerUnigram.class Unigram.class
hadoop jar task3.jar Task3 /user/root/input /user/root/output