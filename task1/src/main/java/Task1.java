import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Task1 {

    private static final String ngramfile1 = "googlebooks-spa-all-1gram-20120701-a";

    /**
     * Main class and 1 job.
     * @param args [0] input directory [1] output directory.
     */
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Arguments: <input> <output>");
            System.exit(1);
        }

        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Task1A - version 1 - 1gram count");

        // Set class
        job.setJarByClass(Task1.class);
        job.setMapperClass(MapperUnigram.class);
        job.setCombinerClass(ReducerUnigram.class);
        job.setReducerClass(ReducerUnigram.class);

        // Specify the type of output keys and values
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Unigram.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Unigram.class);

        // Set files (input - output)
        FileInputFormat.addInputPath(job, new Path(args[0] + "/" + ngramfile1));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}