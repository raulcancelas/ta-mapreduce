import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Unigram class
 */
public class Unigram implements Writable {
    private IntWritable occurs;
    private Text ngram;

    /**
     * Constructor
     */
    public Unigram() {
        setOccurs(new IntWritable());
        setNgram(new Text());
    }

    /**
     * Construnctor
     * @param occurs Number of unigrams
     * @param ngram Unigram
     */
    public Unigram(int occurs, String ngram) {
        this.occurs = new IntWritable(occurs);
        this.ngram = new Text(ngram);
    }

    public int getOccurs() {
        return occurs.get();
    }

    public void setOccurs(IntWritable occurs) {
        this.occurs = occurs;
    }

    public String getNgram() {
        return ngram.toString();
    }

    public void setNgram(Text ngram) {
        this.ngram = ngram;
    }

    /**
     * Tu string
     * @return
     */
    public String toString() {
        return ngram.toString() + "\t" + occurs.toString();
    }

    /**
     * Write (Needed by Writable)
     * @param dataOutput
     * @throws IOException
     */
    public void write(DataOutput dataOutput) throws IOException {
        occurs.write(dataOutput);
        ngram.write(dataOutput);
    }

    /**
     * readFields (Needed by Writable)
     * @param dataInput
     * @throws IOException
     */
    public void readFields(DataInput dataInput) throws IOException {
        occurs.readFields(dataInput);
        ngram.readFields(dataInput);
    }
}
