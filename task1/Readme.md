## PART 1 - LOCAL PSEUDODISTRIBUTED

### Hadoop installation

- I run a pseudodistributed hadoop in a ubuntu:16.04 docker instance.

- I add to the delivery all the automation scripts (hadoop + dockerfiles) -> localpseudodistributed folder.

### Mapreduce algorithm - TASK 1

- In the mapper , I pass each line of the google 1gram file with the correct decade, for example

```
  1981 a 23    ->   1980 a 23 
  1982 a 45    ->   1980 a 45
  1997 a 2     ->   1990 a 2
  1988 a 27    ->   1990 a 27
```

- In the reducer, add all the equals words in the same decade and choose the best (if tie, alphabetical order):

```
  1980 [a 23, a 45, arbol 34, ... ] -> 1980 [a 68, arbol 34, .. ] -> 1980 a 68
```

### Mapreduce algorithm - TASK 2

- First Stage, the same as TASK 1.

- Second Stage:

Check the most repeat words in a result temporal file, and then call the second mapper with the best words per decade in tuples, in the Configuration class. Also get the required or files in the input folder (bigram files).

Then in the mapper check if the first ngram is the most repeat and put the lines like the first mapper:

```
A the most repeat:

  1981 a arbol 23    ->   1980 a arbol 23 
  1982 a arbol 45    ->   1980 a arbol 45
  1997 a mesa  2     ->   1990 a mesa  2
  1988 b arbol 27    x
```

The reduce is like the first stage.



### Result

- TASK 1 (Time local pseudodistributed: 2:50 minutes)

```
1800	á	3033113
1810	á	1860063
1820	á	3730049
1830	á	4416875
1840	á	9772045
1850	á	12072407
1860	á	16398777
1870	á	12461886
1880	á	14821558
1890	á	14784157
1900	á	19866126
1910	a	16691904
1920	a	31139334
1930	a	40833875
1940	a	62487629
1950	a	74791153
1960	a	111785626
1970	a	143284924
1980	a	176316761
1990	a	224358924
2000	a	277392143
```

- TASK 2 (Time local pseudodistributed: 5:00 minutes)

```
1900	á _DET_	10479806
1910	a _DET_	8206832
1920	a _DET_	15653312
1930	a _DET_	20580790
1940	a _DET_	31622106
1950	a _DET_	37577188
1960	a _DET_	56117998
1970	a _DET_	71739051
1980	a _DET_	88812732
1990	a _DET_	112580535
```