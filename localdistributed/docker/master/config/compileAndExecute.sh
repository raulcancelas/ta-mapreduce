#!/bin/bash
hdfs dfs -rm -r /user/root/output
hadoop com.sun.tools.javac.Main Task1.java MapperUnigram.java ReducerUnigram.java Unigram.java
jar cf task.jar Task1.class MapperUnigram.class ReducerUnigram.class Unigram.class
yarn jar task.jar Task1 /user/root/input /user/root/output