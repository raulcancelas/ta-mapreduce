#!/bin/bash

# create base hadoop cluster docker image
docker build -f docker/base/Dockerfile -t rcf/hadoop-cluster-base:latest docker/base

# create master node hadoop cluster docker image
docker build -f docker/master/Dockerfile -t rcf/hadoop-cluster-master:latest docker/master

docker network create --driver=bridge hadoop &> /dev/null

# start hadoop slave container
counter=1
while [ $counter -le 2 ]
do
	docker rm -f hadoop-slave$counter &> /dev/null
	echo "start hadoop-slave$counter container..."
	docker run -itd \
	                --net=hadoop \
	                --name hadoop-slave$counter \
	                --hostname hadoop-slave$counter \
	                rcf/hadoop-cluster-base
	((counter++))
done 



# start hadoop master container
docker rm -f hadoop-master &> /dev/null
echo "start hadoop-master container..."
docker run -itd \
                --net=hadoop \
                -p 50070:50070 \
                -p 8088:8088 \
                -p 9870:9870 \
                -p 20:22 \
                --name hadoop-master \
                --hostname hadoop-master \
				-v $PWD/data:/data \
                rcf/hadoop-cluster-master


echo "########################"
echo "DOCKER BASH MASTER: docker exec -it hadoop-master bash"
echo "SSH SESSION: ssh tauser@localhost -p 20 || password: qwe123"
echo "########################"
echo "execute: /etc/configure-hdfs-files.sh"

# Update code
./updateCode.sh $1

# get into hadoop master container
docker exec -it hadoop-master bash
