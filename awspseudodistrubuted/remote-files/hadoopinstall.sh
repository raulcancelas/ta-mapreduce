#!/bin/bash

## install components
sudo apt-get update && sudo apt-get install -y openssh-server ssh openjdk-8-jdk wget

## download hadoop
wget http://apache.rediris.es/hadoop/common/hadoop-3.2.1/hadoop-3.2.1.tar.gz
tar -xzvf hadoop-3.2.1.tar.gz
sudo mv hadoop-3.2.1 /usr/local/hadoop
rm hadoop-3.2.1.tar.gz

# install lzo codec
sudo apt-get update && sudo apt-get install -yq  liblzo2-2 liblzo2-dev 

# export environment variables
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export HADOOP_CLASSPATH=/usr/lib/jvm/java-8-openjdk-amd64/lib/tools.jar
export HADOOP_HOME=/usr/local/hadoop
export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:$HADOOP_HOME/share/hadoop/tools/lib/*
export PATH=$PATH:/usr/local/hadoop/bin:/usr/local/hadoop/sbin
export PATH=$PATH:$HADOOP_HOME/share/hadoop/tools/lib/*

# ssh
ssh-keygen -t rsa -f ~/.ssh/id_rsa -P ''
    cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
    chmod 0600 ~/.ssh/authorized_keys

mkdir ~/code

mkdir -p ~/hdfs/namenode
mkdir -p ~/hdfs/datanode
mkdir $HADOOP_HOME/logs

# Copy conf files
mv ~/config/ssh_config ~/.ssh/config
mv ~/config/hadoop-env.sh /usr/local/hadoop/etc/hadoop/hadoop-env.sh
mv ~/config/hdfs-site.xml $HADOOP_HOME/etc/hadoop/hdfs-site.xml
mv ~/config/core-site.xml $HADOOP_HOME/etc/hadoop/core-site.xml
mv ~/config/mapred-site.xml $HADOOP_HOME/etc/hadoop/mapred-site.xml
mv ~/config/yarn-site.xml $HADOOP_HOME/etc/hadoop/yarn-site.xml
mv ~/config/showResults.sh ~/code/showResults.sh
mv ~/config/compileAndExecute3.sh ~/compileAndExecute3.sh
mv ~/config/hadoop-lzo-0.4.21-SNAPSHOT.jar /usr/local/hadoop/share/hadoop/common/hadoop-lzo-0.4.21-SNAPSHOT.jar