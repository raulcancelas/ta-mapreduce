#!/bin/bash

echo -e "\n"

hdfs namenode -format

echo -e "\n"

$HADOOP_HOME/sbin/start-dfs.sh
$HADOOP_HOME/sbin/start-yarn.sh

echo -e "\n"

