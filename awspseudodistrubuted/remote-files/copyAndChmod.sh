#!/bin/bash

# Copy conf files
mv ~/config/ssh_config ~/.ssh/config
mv ~/config/hadoop-env.sh /usr/local/hadoop/etc/hadoop/hadoop-env.sh
mv ~/config/hdfs-site.xml $HADOOP_HOME/etc/hadoop/hdfs-site.xml
mv ~/config/core-site.xml $HADOOP_HOME/etc/hadoop/core-site.xml
mv ~/config/mapred-site.xml $HADOOP_HOME/etc/hadoop/mapred-site.xml
mv ~/config/yarn-site.xml $HADOOP_HOME/etc/hadoop/yarn-site.xml
mv ~/config/showResults.sh ~/code/showResults.sh
mv ~/config/compileAndExecute3.sh ~/code/compileAndExecute3.sh
mv ~/config/hadoop-lzo-0.4.21-SNAPSHOT.jar /usr/local/hadoop/share/hadoop/common/hadoop-lzo-0.4.21-SNAPSHOT.jar

chmod +x ~/hadoopinstall.sh
chmod +x ~/start-hadoop.sh
chmod +x ~/stop-hadoop.sh
chmod +x ~/code/compileAndExecute.sh
chmod +x ~/code/showResults.sh
